#!/usr/bin/env python
# -*- coding: utf-8 -*-
import codecs
import os
import re

from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))

# To update the package version number,
# edit unittesten/__init__.py


def read(*parts):
    with codecs.open(os.path.join(here, *parts), "r") as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


with open("README.rst") as readme_file:
    readme = readme_file.read()

setup(
    name="unittesten",
    version=find_version("unittesten", "__init__.py"),
    description="Leer unittests schrijven met dit project!",
    long_description=readme + "\n\n",
    author="Anna Baas",
    author_email="anna.baas@rws.nl",
    url="https://git.intranet.rws.nl/rwsdatalab/guides/unittesten",
    packages=["unittesten", "unittesten/stap1", "unittesten/stap2"],
    include_package_data=True,
    zip_safe=False,
    keywords="unittesten",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    test_suite="tests",
    install_requires=[],
    setup_requires=[
        # dependency for `python setup.py test`
        "pytest-runner",
        # dependencies for `python setup.py build_sphinx`
        "sphinx",
        "sphinx_rtd_theme",
        "recommonmark",
    ],
    tests_require=["pytest", "pytest-cov"],
    extras_require={
        "dev": [
            "bandit",
            "black",
            "flake8",
            "flake8-bugbear",
            "flake8-comprehensions",
            "flake8-docstrings",
            "isort",
            "pre-commit",
            "pylint",
            "pylint[prospector]",
            "pytest",
            "pytest-cov",
            "radon",
            "safety",
        ],
    },
)
