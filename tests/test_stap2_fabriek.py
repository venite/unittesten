import pytest
from unittesten.stap2.fabriek import Fabriek
from unittesten.stap2.fabrieksobject import Fabrieksobject


class TestFabriek:
    """Test class for fabriek class"""

    def test_lees_objecten(self):
        """Test reading an object."""
        fabriek = Fabriek()
        expected_object = Fabrieksobject(12, "blauw", "kubus", 36)
        testpath = "tests/testdata/objecten.csv"
        for object in fabriek.lees_objecten(testpath):
            assert expected_object.to_dict() == object.to_dict()
            break

    def test_schrijf_objecten(self):
        pass

    def test_controleer_object(self):
        """Test checking the objects for client acceptance."""
        fabriek = Fabriek()
        acceptable_object = Fabrieksobject(12, "blauw", "kubus", 36)
        unacceptable_object = Fabrieksobject(12, "geel", "kubus", 36)

        # assert fabriek.controleer_object(acceptable_object) is True
        # assert fabriek.controleer_object(unacceptable_object) is False

    def test_sorteer_objecten(self):
        """Test sorting the objects."""
        fabriek = Fabriek()
        blauw_object_1 = Fabrieksobject(12, "blauw", "kubus", 36)
        blauw_object_2 = Fabrieksobject(12, "blauw", "kegel", 4)
        geel_object_1 = Fabrieksobject(12, "geel", "kubus", 16)
        geel_object_2 = Fabrieksobject(12, "geel", "kubus", 9)

        input = [blauw_object_1, geel_object_1, geel_object_2, blauw_object_2]
        verwachte_output = [
            blauw_object_1,
            blauw_object_2,
            geel_object_1,
            geel_object_2,
        ]

        # assert fabriek.sorteer_objecten(input) == verwachte_output

    def test_verwerk_objecten(self):
        pass
