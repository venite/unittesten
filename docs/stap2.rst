======
Stap 2
======
Bekijk de code in het package /unittesten/stap2.
Deze code is al gedeeltelijk opgedeeld in verschillende methodes.
Een deel van de methodes is nog niet ingevuld.


Tests
=====
In de directory tests staan unittests voor de nog niet ingevulde methodes.
De assertions staan nog in commentaar. Haal ze uit commentaar.

Draai de tests met

.. code-block:: console

  python -m setup.py tests

of de test-omgeving van je favoriete editor, als die er een heeft.
De tests falen, want de methodes zijn nog niet geimplementeerd!

Opdracht 1
==========
Implementeer de methodes zodat de tests slagen.

Opdracht 2
===========
Schrijf tests voor de methodes die deze nog niet hebben.

Opdracht 3
===========
Refactor de code zodat de nieuwe methodes gebruikt worden.
