.. unittesten documentation master file, created by
   sphinx-quickstart on Thu Jun 21 11:07:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentatie unittesten oefenpackage
==========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   fabriek
   stap1
   stap2

API Reference
=============

.. toctree::
  :maxdepth: 2

  unittesten <apidocs/unittesten.rst>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
