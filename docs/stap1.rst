======
Stap 1
======
Bekijk fabriek.py in de directory /unittesten/stap1. Run de code.
Hoe zou je deze code kunnen unittesten?
En wat weet je precies als de tests dan falen?

Unittesten
==========
Het idee achter unittesten is dat elk stukje code individueel getest wordt.
Een unittest beschrijft welke input een functie nodig heeft en welke output we daarbij verwachten.
Op die manier kan een unittest voor ontwikkelaars ook als documentatie dienen.

Modulaire code
==============
De code in fabriek.py is moeilijk te testen omdat het alles tegelijk doet:
inlezen, verwerken, en wegschrijven. Als een test faalt weet je dus nog niet waar het misgaat.
Het kan in elk van die stappen gebeurd zijn.

Door code op te delen in methodes die elk maar één ding doen is het veel makkelijker om het testen.

Modulaire code heeft ook voordelen voor de begrijpelijkheid en onderhoudbaarheid van systemen.
Als het inlezen van de data in een aparte methode gebeurt, hoef je alleen die methode aan te passen als de data
op een andere manier aangeleverd wordt. En iemand die geinteresseerd is in hoe de data verwerkt wordt,
maar niet hoeft te weten hoe deze wordt ingelezen of weggeschreven, kan snel de juiste sectie vinden.

Kijk nog eens naar fabriek.py. Hoe is deze code meer modulair te maken? Wat voor tests kun je daar dan bij maken?


Code wijzigen
=============
Ons doel is dat er voor alle code unit tests zijn. Het grote voordeel daarvan is dat je bestaande code
veilig kunt wijzigen. De tests laten dan zien dat de code die je niet hebt aangepast nog volgens verwachting werkt.
Of laten zien dat een wijziging ook op andere plekken gevolgen heeft, bijvoorbeeld doordat de output van de ene functie
de input van een andere functie is.
