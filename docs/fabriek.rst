=======================
De Grote Sorteerfabriek
=======================

In het stadje Datadrecht staat een grote fabriek die objecten sorteert. De objecten worden aangevoerd, uitgepakt, gesorteerd, ingepakt, en weer weggevoerd. Soms komen we ook objecten tegen die de klant niet wil hebben. Die worden naar een andere fabriek doorgestuurd.


De objecten
===========

De objecten worden gerepresenteerd door arrays. Het eerste en laatste element van de arrays zijn verpakking. Het object zelf heeft een nummer, kleur (blauw, geel of rood), vorm (bol, kubus, of kegel) en gewicht.

Voorbeeld
*********

    - ["x", 12, "blauw", "kubus", 36, "y"]
    - ["x", 4, "rood", "bol", 16, "y"]
    - ["x", 4, "geel", "kegel", 9, "y"]

De klant
==========

De fabriek heeft momenteel één klant, Rowena van Veelvlak. Mevrouw van Veelvlak wil de objecten per kleur geleverd krijgen. De volgorde van de kleuren maakt niet uit. Daarnaast wil ze alleen gele objecten als die minder dan 20 wegen en geen rode kegels.
