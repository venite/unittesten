################################################################################
unittesten
################################################################################

Leer unittests schrijven met dit project!


Installation
------------

To install unittesten, do:

.. code-block:: console

  git clone https://git.intranet.rws.nl/rwsdatalab/guides/unittesten.git
  cd unittesten
  pip install .

Run tests (including coverage) with:

.. code-block:: console

  python setup.py test


Development
-----------

The unittesten package enforces code quality through `pre-commit <https://pre-commit.com/`_. The pre-commit
package can be installed via:

.. code-block:: console

    pip install -r requirement-dev.txt

To have pre-commit automatically executed when doing a git commit, run the following command in your git repo:

.. code-block:: console

    pre-commit install

To run pre-commit manually, use the following command:

.. code-block:: console

    pre-commit run -a


Documentation
*************

.. _README:

Include a link to your project's full documentation here.

License
*******

Copyright (c) 2020, Rijkswaterstaat


Credits
*******

This package was created with `Cookiecutter <https://github.com/audreyr/cookiecutter>`_ and the `RWS/python-template <https://git.intranet.rws.nl/harenr/python-template>`_.
