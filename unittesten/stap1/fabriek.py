"""Sorting fabriek."""
import csv


def main():
    """Unpack and sort objects."""
    blue_objects = []
    red_objects = []
    yellow_objects = []
    discarded_objects = []
    # open the csv
    with open("../../../data/objecten.csv", "r", newline="") as inputfile:
        rdr = csv.reader(inputfile, delimiter=",")
        for row in rdr:
            # Remove the wrapping
            del row[0]
            del row[-1]
            # Test whether objects are wanted by customer
            if row[1] == "blauw":
                blue_objects.append(row)
                continue
            if row[1] == "rood" and row[2] != "kegel":
                red_objects.append(row)
                continue
            if row[1] == "geel" and int(row[3]) < 20:
                yellow_objects.append(row)
                continue
            # If you get here, the object is not wanted by the customer
            discarded_objects.append(row)
    with open("../../../data/output.csv", "w", newline="") as outputfile:
        writer = csv.writer(
            outputfile, delimiter=",", quotechar="|", quoting=csv.QUOTE_MINIMAL
        )
        for row in blue_objects:
            writer.writerow(row)
        for row in yellow_objects:
            writer.writerow(row)
        for row in red_objects:
            writer.writerow(row)


if __name__ == "__main__":
    main()
