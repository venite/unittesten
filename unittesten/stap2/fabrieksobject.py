"""Het object dat door de fabriek verwerkt wordt."""


class Fabrieksobject:
    """Representation of the objects to process."""

    def __init__(self, nummer: int, kleur: str, vorm: str, gewicht: int):
        self._nummer = nummer
        self._kleur = kleur
        self._vorm = vorm
        self._gewicht = gewicht

    @property
    def nummer(self):
        """Getter: External identifier."""
        return self._nummer

    @property
    def kleur(self):
        """Getter: colour."""
        return self._kleur

    @property
    def vorm(self):
        """Getter: shape."""
        return self._vorm

    @property
    def gewicht(self):
        """Getter: weight."""
        return self._gewicht

    def to_dict(self):
        """Return a dict with the specs of this object."""
        return {
            "nummer": self._nummer,
            "kleur": self._kleur,
            "vorm": self._vorm,
            "gewicht": self._gewicht,
        }
