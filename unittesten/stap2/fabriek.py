"""Main module for selecting and sorting objects."""
import csv

from unittesten.stap2.fabrieksobject import Fabrieksobject


class Fabriek:
    """Class for selecting and sorting objects."""

    def lees_objecten(self, path):
        """Read the objects from the given path. Discard the packaging."""
        with open(path, "r", newline="") as inputfile:
            rdr = csv.reader(inputfile, delimiter=",")
            for row in rdr:
                yield Fabrieksobject(int(row[1]), row[2], row[3], int(row[4]))

    def schrijf_objecten(self, path, objecten: [Fabrieksobject]):
        """Write the given objects to the given path."""
        field_names = ["nummer", "kleur", "vorm", "gewicht"]
        with open(path, "w", newline="") as outputfile:
            writer = csv.DictWriter(outputfile, delimiter=",", fieldnames=field_names)
            for fabrieksobject in objecten:
                writer.writerow(fabrieksobject.to_dict())

    def controleer_object(self, object: Fabrieksobject):
        """Check whether the object complies with client standards."""
        pass

    def sorteer_objecten(self, objecten: [Fabrieksobject]):
        """Sort the objects according to client standards."""
        pass

    def verwerk_objecten(self, input_path: str, output_path: str):
        """Unpack and sort objects."""
        wanted_objects = []
        discarded_objects = []
        for object in self.lees_objecten(input_path):
            if object.kleur == "blauw":
                wanted_objects.append(object)
                continue
            if object.kleur == "rood" and object.vorm != "kegel":
                wanted_objects.append(object)
                continue
            if object.kleur == "geel" and object.gewicht < 20:
                wanted_objects.append(object)
                continue
            # If you get here, the object is not wanted by the customer
            discarded_objects.append(object)
        wanted_objects.sort(key=lambda fabrieksobject: fabrieksobject.kleur)
        self.schrijf_objecten(output_path, wanted_objects)
