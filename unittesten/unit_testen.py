#!/usr/bin/env python
"""Run the (step2) Fabriek module."""

from unittesten.stap2.fabriek import Fabriek


def main():
    """Run the simulation."""
    fabriek = Fabriek()
    fabriek.verwerk_objecten("../../data/objecten.csv", "../../data/output.csv")
