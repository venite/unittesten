# -*- coding: utf-8 -*-
"""Documentation about unittesten"""
import logging

logging.getLogger(__name__).addHandler(logging.NullHandler())

__author__ = "Anna Baas"
__email__ = "anna.baas@rws.nl"
__version__ = "0.1.0"
